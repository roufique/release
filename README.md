# Gitlab Security Products Release

This repository contains instructions and tools for releasing new versions of
GitLab Security Products, which are developed and maintained by the
[Secure](https://about.gitlab.com/handbook/engineering/ops-backend/secure/)
team.

The goal is to provide clear instructions and procedures for our entire release
process, along with automated tools, to help anyone perform the role of Release
Manager.

## Guides

- [Release Process](docs/release_process.md)
- [Release Managers](docs/release_managers.md)
- [Reaction Rotation](docs/reaction.md)
- [QA Process](docs/qa_process.md)

## Contributing

See [CONTRIBUTING.md](./CONTRIBUTING.md).

## License

See [LICENSE](./LICENSE).
