# Reaction Rotation (WIP)

_WIP Note_: This process is a project, and will be effective only when this [position](https://about.gitlab.com/jobs/apply/?gh_jid=4055697002) is filled.

## Purpose

This process is inspired by the similar [page of the handbook](https://about.gitlab.com/handbook/engineering/reaction/),
and is specific to the [Secure](https://about.gitlab.com/handbook/engineering/ops-backend/secure/) team.
As we now want to commit to 100% deliverables shipped in each iteration, the team must be completely focusing on this objective.
There are many sources of potential interrupt-driven work that can impact our planning.
They are hard to evaluate in advance and sometimes impossible to anticipate, and we want to avoid context switching as much as possible.

## Rotation Scheduling

Every release, a Release Manager is assigned, and at least [one release in advance](./release_managers.md).
We decide during the Weekly meeting following the code freeze, taking into account vacations or other absences.

## Responsibilities

The [Secure Release Manager](./release_managers.md) is dedicated completely to this process, which means he/she must not take any ~Deliverable feature during the iteration.

Note: ~bug and ~regression are labelled with ~deliverable. In this case, the
[Secure Release Manager](./release_managers.md) **must** be assigned,
unless the Engineering Manager already assigned someone else from the team instead.
Some bugs/regressions are easier to fix for the developer who just wrote the feature.

The top priority is given to the [release process](/docs/release_process.md),
but this task only occurs around the code freeze date.
Therefore, on the remaining available time, the responsibilities of the
[Secure Release Manager](./release_managers.md) are (order by priority desc):

- Fill our [Advisories DB](https://gitlab.com/gitlab-org/security-products/advisories)
- Shoot bugs and assist the support team
- Update the external tools we wrap for [SAST](https://docs.gitlab.com/ee/user/project/merge_requests/sast.html), [DAST](https://docs.gitlab.com/ee/user/project/merge_requests/dast.html), ...
- Manage opensource contributions to our projects
- Maintain Gemnasium code
- Improve documentation
- Keep the release process up-to-date
- Maintain [QA](https://gitlab.com/gitlab-org/security-products/release/blob/master/docs/qa_process.md) and make sure all our projects are covered
