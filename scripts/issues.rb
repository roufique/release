# frozen_string_literal: true

require 'gitlab'
require 'versionomy'
require 'yaml'

module Gitlab
  class Client
    # Issues helper
    module Issues
      def group_issues(group = nil, options = {})
        get("/groups/#{url_encode group}/issues", query: options)
      end
    end
  end
end

# Utility module to load Secure merge requests for a release
module Issues
  def self.configure
    Gitlab.configure do |config|
      config.endpoint       = 'https://gitlab.com/api/v4'
      config.private_token  = ENV['GITLAB_API_PRIVATE_TOKEN']
    end
  end

  def self.load(milestone)
    configure

    Gitlab.group_issues('gitlab-org',
                        labels: 'Secure',
                        milestone: milestone.to_s,
                        state: 'closed',
                        per_page: 99_999)
  end
end
