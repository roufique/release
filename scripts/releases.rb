# frozen_string_literal: true

# Utility module for release management
module Releases
  # Raise an exception if the the release doesn't have theright format (Major.Minor)
  def self.validate!(release)
    raise "invalid release: #{release}, must be X.Y" unless release.is_a?(String) && release.match(/^\d+\.\d+$/)
  end
end
