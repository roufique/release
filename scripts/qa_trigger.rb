#!/usr/bin/env ruby
# frozen_string_literal: true

# QA trigger script launch a pipeline on each test project to validate features are
# still working as expected.
# By default the pipelines are triggered on the QA branch but you can specify one
# Usage:
# ./scripts/qa_trigger.rb [branch]

require 'yaml'
require 'json'
require 'net/http'
require 'uri'

QA_BRANCH = 'QA-all-base-FREEZE'
TESTS_REPO_NAMESPACE = 'gitlab-org/security-products/tests'

repos = YAML.load_file(File.expand_path('./test_projects.yml', __dir__))

def self.trigger_repo(id, token, branch)
  uri = URI.parse("https://gitlab.com/api/v4/projects/#{id}/trigger/pipeline")
  params = {
    token: token,
    ref: branch
  }

  https = Net::HTTP.new(uri.host, uri.port)
  https.use_ssl = true

  req = Net::HTTP::Post.new(uri.path)
  req.set_form_data(params)

  https.request(req)
end

def self.pipeline_url(repo_name, pipeline_id)
  "https://gitlab.com/#{TESTS_REPO_NAMESPACE}/#{repo_name}/pipelines/#{pipeline_id}"
end

branch = ARGV[0] || QA_BRANCH
errors = []
repos.each do |repo|
  res = trigger_repo(repo['id'], repo['qa_trigger_token'], branch)
  if res.code != '201'
    puts "ERROR: Trigger for project '#{repo['name']}' failed: #{res.inspect}"
    errors << repo['name']
    next
  end

  pipeline = JSON.parse(res.body)
  puts "SUCCESS: Pipeline triggered for project '#{repo['name']}': " \
       "#{pipeline_url(repo['name'], pipeline['id'])}"
end

puts "\nTrigger failed for some projects: #{errors.join(', ')}" if errors.any?
